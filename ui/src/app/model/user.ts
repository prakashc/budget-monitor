export interface User {
    id: string;
    firstname: string;
    lastname: string;
    email: number;
    token: string;
}
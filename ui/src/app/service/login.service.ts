import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, shareReplay } from 'rxjs/operators';
import { stringify } from 'querystring';

const endpoint = 'http://localhost:8082/';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  loginUser(username: string, password: string): Observable<any> {
    return this.http.post<any>(endpoint + 'authenticate', {username, password}).pipe(
      catchError(this.handleError)
    );
  }

  signupUser(user: any): Observable<any> {
    return this.http.post(endpoint + 'signup', user).pipe(
      catchError(this.handleError)
    );
  }

  getMessage(): Observable<any> {
    console.log('reached getMessage()');
    return this.http.get(endpoint + 'hello', { responseType: 'text'}).pipe(
      catchError(this.handleError)
    );
  }

}

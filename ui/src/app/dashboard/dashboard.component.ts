import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  message: string;

  constructor(private service: LoginService) {
  }

  ngOnInit(): void {
      this.service.getMessage()
        .subscribe((response: any) => {
          console.log('res in dashboard:'+response);
          this.message = response;
      });
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../model/user';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private service: LoginService) {
  }

  ngOnInit(): void {
  }

  user: User;

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  signupForm = new FormGroup({
    firstname: new FormControl('', [Validators.required]),
    lastname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    password1: new FormControl('', [Validators.required]),
    password2: new FormControl('', [Validators.required]),
  });

  onLoginSubmit() {
    this.service.loginUser(this.loginForm.value['username'], this.loginForm.value['password'])
          .subscribe((response: any) => {
      sessionStorage.setItem('token', response['token']);
      if(!(response === null)){
        this.router.navigate(['/dashboard']);
      }
    });
  }

  onSignupSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.signupForm.value);
  }

}
